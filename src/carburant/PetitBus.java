package carburant;

public class PetitBus extends Vehicule {

	 final int pav = 4000;
	 public int vitesseMax = 150;

	public PetitBus(String immat) {
		super(immat);
	}

	@Override
	public String toString() {
		return super.toString() + "\nc'est un Petit Bus dont le poids a vide est de " + pav + " kgs, sa vitesse max est de " + vitesseMax + " km/h";
	}

	
}
