package carburant;

public class Vehicule {

	protected String immat;
	protected int vitesseMax, chargement;

	public Vehicule(String immat) {
		this.immat = immat;
	}

	public Vehicule(String immat, int chargement) {
		this.immat = immat;
		this.chargement = chargement;
	}

	public String getImmat() {
		return immat;
	}

	public void setImmat(String immat) {
		this.immat = immat;
	}


	public int getVitesseMAx() {
		return vitesseMax;
	}

	public void setVitesseMAx(int vitesseMAx) {
		this.vitesseMax = vitesseMAx;
	}

	public int isChargement() {
		return chargement;
	}

	public void setChargement(int chargement) {
		this.chargement = chargement;
	}

	public int calculVitesse() {
		int vitesseMax = 0;
		return vitesseMax;

	}

	public void afficherVehicule() {
		System.out.println(toString());
	}

	@Override
	public String toString() {
		return "il sagit d'un vehicule ayant l'immatriculation => " + immat;
	}

	

}
