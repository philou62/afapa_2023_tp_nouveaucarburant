package carburant;

public class CamionBache extends Vehicule {
	
	final int pav = 4000;
	final int chargeMax = 20000;
	

	public CamionBache(String immat, int chargement) {
		super(immat, chargement);	
	}

	public int calculVitesse() {
		if (chargement == 0 ) {
			vitesseMax = 130;
		} else if (chargement <= 3000){
			vitesseMax = 110;
		} else if (chargement > 3000 && chargement < 7000) {
			vitesseMax = 90;
		} else if (chargement > 7000) {
			vitesseMax = 80;
		}
		return vitesseMax;
	}

	@Override
	public String toString() {
		return super.toString() + "\n c'est un camion bache dont le poids a vide " + pav + " sa  charge max est de " + chargeMax + "kgs vitessemax=" + 	calculVitesse() + "]";
	}
	

}
