package carburant;

import java.util.*;


public class Convoi {
	public static void main(String[] args) {
		ArrayList<Vehicule> convoi = new ArrayList<>();
		convoi.add(new CamionBache("555 SO 66", 4000));
		convoi.add(new CamionCiterne("547 GG 62", 547));
		convoi.add(new PetitBus("666 QQ 59"));
		
		ArrayList<Vehicule> convoi1 = new ArrayList<>();
		convoi1.add(new CamionCiterne("654 VV 62", 0));
		convoi1.add(new CamionCiterne("987 CC 95", 0));
		
		System.out.println("****************- CONVOI A -***************");
		for (Vehicule convoiA : convoi) {
			System.out.println(convoiA + "\n");
		}
		
		System.out.println("****************- CONVOI B -***************");
		
		for (Vehicule convoiB : convoi1)
			System.out.println(convoiB + "\n");
	}
	
}

