package carburant;

public class TestVehicule {

	public static void main(String[] args) {
		
		PetitBus bus1 = new PetitBus("525 QQ 62");
		bus1.afficherVehicule();
		System.out.println("****************************************************************");
		CamionCiterne Cc = new CamionCiterne("445 kk 52", 6000);
		Cc.afficherVehicule();
		System.out.println("****************************************************************");

		CamionBache cB = new CamionBache("555 DD 626", 7900);
		cB.afficherVehicule();
		
		CamionBache cB2 = new CamionBache("837 GG 555", 8000);
		cB2.afficherVehicule();
	}

}
