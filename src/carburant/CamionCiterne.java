package carburant;

public class CamionCiterne extends Vehicule{
	
	 final int chargeMax = 10000;
	 final int pav = 3000;
	
	
	public CamionCiterne(String immat, int chargement) {
		super(immat, chargement);
		
	}

	public int calculVitesse() {
		if (chargement == 0 ) {
			vitesseMax = 130;
		} else if (chargement <= 1000){
			vitesseMax = 110;
		} else if (chargement > 1000 && chargement < 4000) {
			vitesseMax = 90;
		} else if (chargement > 4000) {
			vitesseMax = 80;
		}
		return vitesseMax;
	}

	@Override
	public String toString() {
		return super.toString() + "\nc'est un Camion Citerne dont le poids a vide est de " + pav + " kgs\nsa charge Maxi est de " + chargeMax + "kgs et, il a un chargement actuelle de " + chargement + " kgs, sa vitesse max est donc de " + calculVitesse() + " km/h";
	}	

}
